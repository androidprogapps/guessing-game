package com.harish.guessinggame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    val words = listOf("Android", "Harish", "Raam")
    val secretWord = words.random().uppercase()
    var correctGuesses = ""

    private val _secretWordDisplay = MutableLiveData("")
    val secretWordDisplay : LiveData<String>
        get() = _secretWordDisplay

    private val _incorrectGuesses = MutableLiveData("")
    val incorrectGuesses : LiveData<String>
        get() = _incorrectGuesses

    private val _livesLeft = MutableLiveData(8)
    val livesLeft : LiveData<Int>
        get() = _livesLeft

    private val _gameOver = MutableLiveData(false)
    val gameOver : LiveData<Boolean>
        get() = _gameOver


    init {
        _secretWordDisplay.value = deriveSecretWordDisplay()
    }

    private fun deriveSecretWordDisplay(): String {
        var display = ""
        secretWord.forEach {
            display += checkLetter(it.toString())
        }
        return display
    }

    private fun checkLetter(str: String) = when (correctGuesses.contains(str)) {
        true -> str
        false -> "_"
    }

    fun makeGuess(guess: String) {
        if (guess.length == 1) {
            if (secretWord.contains(guess)) {
                correctGuesses += guess
                _secretWordDisplay.value = deriveSecretWordDisplay()
            } else {
                _incorrectGuesses.value += "$guess "
                _livesLeft.value = _livesLeft.value?.minus(1)
            }
            _gameOver.value = isWon() || isLost() // Assigns true if the game isWon() or isLost()
        }
    }

    private fun isWon() = secretWord.equals(secretWordDisplay.value, true)
    private fun isLost() = (livesLeft.value ?: 0) <= 0

    fun wonLostMsg(): String {
        var msg = ""
        if (isWon()) msg = "You won!"
        else if (isLost()) msg = "You lost!"
        msg += "The word was $secretWord"
        return msg
    }
}